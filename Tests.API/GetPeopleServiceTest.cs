﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Workshop20170916.Demo.Services;

namespace Tests.API
{
	[TestClass]
	public class GetPeopleServiceTest
	{
		[TestMethod]
		public void GetListOfPeople()
		{
			var service = new PeopleService();
			var resultCount = 11;
			var res = service.GetPeopleList(new
			{
				results = resultCount,
			});

			Assert.IsTrue(res.Result.Results.Count == resultCount);
		}
	}
}
