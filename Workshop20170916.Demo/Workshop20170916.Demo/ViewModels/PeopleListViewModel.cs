﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using Workshop20170916.Demo.Services;

namespace Workshop20170916.Demo.ViewModels
{
	public class PeopleListViewModel : BindableBase, INavigationAware
	{
		private readonly IWebService _peopleService;
		public List<People> Peoples { get; set; }

		public PeopleListViewModel(IWebService peopleService)
		{
			_peopleService = peopleService;
		}

		public void OnNavigatedFrom(NavigationParameters parameters)
		{
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			Peoples = (_peopleService as PeopleService)?.GetPeopleList(new
			{
				results = 10
			}).Result.Results;
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{
		}
	}
}
