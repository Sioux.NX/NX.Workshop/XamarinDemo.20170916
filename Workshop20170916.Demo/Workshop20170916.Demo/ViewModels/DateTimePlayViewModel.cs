﻿using Prism.Navigation;

namespace Workshop20170916.Demo.ViewModels
{
	public class DateTimePlayViewModel: INavigationAware
	{
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{
		}
	}
}