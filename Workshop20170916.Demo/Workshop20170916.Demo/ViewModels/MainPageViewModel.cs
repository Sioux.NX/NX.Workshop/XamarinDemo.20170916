﻿using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Workshop20170916.Demo.Services;

namespace Workshop20170916.Demo.ViewModels
{
	public class MainPageViewModel : BindableBase, INavigationAware
	{

		private string _username;
		private string _welcomeText;
		private bool _showNextButton;
		private INavigationService _navigationService;

		public DelegateCommand NextCommand { get; private set; }

		public bool ShowNextButton
		{
			get { return _showNextButton; }
			set { SetProperty(ref _showNextButton, value); }
		}

		public string WelcomeText
		{
			get { return _welcomeText; }
			set
			{
				SetProperty(ref _welcomeText, value); 
			}
		}

		public MainPageViewModel(INavigationService navigationService)
		{
			NextCommand = new DelegateCommand(OnNextCommand, CanExcuteNextCommand).ObservesProperty(() => Username);
			_navigationService = navigationService;
		}

		private bool CanExcuteNextCommand()
		{
			return Username.Length >= 5;
		}

		private void OnNextCommand()
		{
			_navigationService.NavigateAsync("NavigationPage/PlaygroundPage");
		}

		public string Username
		{
			get { return _username; }
			set
			{
				SetProperty(ref _username, value);

				if (Username.Length >= 5)
				{
					SetProperty(ref _welcomeText, $"Hello, {Username}, that's a good name");
					SetProperty(ref _showNextButton, true);

					RaisePropertyChanged(nameof(WelcomeText));
					RaisePropertyChanged(nameof(ShowNextButton));
				}
				else
				{
					SetProperty(ref _welcomeText, "");
					SetProperty(ref _showNextButton, false);

					RaisePropertyChanged(nameof(WelcomeText));
					RaisePropertyChanged(nameof(ShowNextButton));
				}
			}
		}

		public void OnNavigatedFrom(NavigationParameters parameters)
		{
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			if (parameters.ContainsKey("username"))
				Username = (string) parameters["username"];
		}
	}
}