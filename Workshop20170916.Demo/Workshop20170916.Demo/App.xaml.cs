﻿using Autofac;
using Prism.Autofac;
using Prism.Autofac.Forms;
using Workshop20170916.Demo.Services;
using Workshop20170916.Demo.Views;
using Xamarin.Forms;
using PeopleList = Workshop20170916.Demo.Views.PeopleList;

namespace Workshop20170916.Demo
{
	public partial class App : PrismApplication
	{
		public App(IPlatformInitializer initializer = null) : base(initializer)
		{
		}

		protected override void OnInitialized()
		{
			InitializeComponent();

			NavigationService.NavigateAsync("NavigationPage/MainPage?username=");
		}

		protected override void RegisterTypes()
		{
			Container.RegisterTypeForNavigation<NavigationPage>();
			Container.RegisterTypeForNavigation<MainPage>();
			Container.RegisterTypeForNavigation<PeopleList>();

			//SERVICES
			RegisterServices();
			Container.RegisterTypeForNavigation<PlaygroundPage>();
		}

		protected void RegisterServices()
		{
			var builder = new ContainerBuilder();
			builder.RegisterType<PeopleService>().As<IWebService>();
			builder.Update(Container);
		}
	}
}