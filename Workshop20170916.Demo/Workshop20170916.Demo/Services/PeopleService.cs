﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;

namespace Workshop20170916.Demo.Services
{
	public class PeopleService : IWebService
	{
		public async Task<PeopleList> GetPeopleList(object @params)
		{
			var result = await "https://randomuser.me/api/".SetQueryParams(@params).GetJsonAsync<PeopleList>();

			return result;
		}
	}
}